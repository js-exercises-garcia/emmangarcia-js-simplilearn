console.log("for loop");

// forloop
 let a =''; //declare ='' to remove undefined;
 for(let x=0; x<5; x++){
    
    console.log(x)
    a += "<h4>"+x+"</h4>";
    document.getElementById('forloop').innerHTML = a

 }

// whileloop
// syntax: while(variable < endCondition)
// variable: "is the value that will change"
//endCondition: "Maximum value that the variable can reach"
console.log("");
 console.log("while loop");
let b =''; //declare ='' to remove undefined;
let y = 1;
while(y<=6 ){
   console.log(y)
   b += "<h4>"+y+"</h4>";
   y++
   document.getElementById('whileloop').innerHTML = b
    
}



// do whileloop
// syntax: while(variable < endCondition)
// variable: "is the value that will change"
//endCondition: "Maximum value that the variable can reach"
console.log("");
 console.log("do while loop");
let c =''; //declare ='' to remove undefined;
let z = 5;
do{
    console.log(z)
    c += "<h4>"+z+"</h4>";
    z++
    document.getElementById('dowhileloop').innerHTML = c
}while(z < 10)


// breakaloop
console.log("");
console.log("break a loop");
let d =''; //declare ='' to remove undefined;

for(let j = 1;j<=10; j++ ){
   console.log(j)
   d += "<h4>"+j+"</h4>";

   document.getElementById('breakaloop').innerHTML = d

   if (j == 5){
    break;
   }
    
}


//skippaloop

console.log("");
console.log("skip a loop");
let e =''; //declare ='' to remove undefined;

for(let h = 1;h<=10; h++ ){
    if (h == 5){
        console.log("skipped "+ h);
        continue;
        
       }
   console.log(h)
   e += "<h4>"+h+"</h4>";

   document.getElementById('skippaloop').innerHTML = e

   
    
}