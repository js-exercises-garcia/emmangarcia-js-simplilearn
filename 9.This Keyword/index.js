// var value = 50;

// function addFixNumber() {
//     let value = 100;
//     alert("Value: " + value);
//     alert("this value " + this.value);
    
// }
// addFixNumber();

//example 2
// const random = {
//     name: "John",
//     info(){
//         console.log("Hi My name is "+ this.name);
//     }
// }
// random.info();

const random = {
    name: "Tutorial",
    video: ["Javascript", "This", "Keyword"],
    info(){
        this.video.forEach(function(tag){
            console.log(this.name,tag)
        },this)
      
    }
}
random.info();
