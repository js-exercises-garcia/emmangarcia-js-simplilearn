function myFirstFunction(val) {
    //displays square of a number
    document.getElementById("demo").innerHTML = val*val
}
myFirstFunction(5)


    let x = function(){
   // alert("Test")
   return 'This was an alert'
}
document.getElementById("demo2").innerHTML = x()

//Func Constructor
let myThirdFunc = new Function('a','b','return a+b')
let y = myThirdFunc(2,3)
document.getElementById("demo3").innerHTML = y


//self invoking function.. see html

// (function(){
//     alert("Lorem")
   
// })();


// Functions as values 
function myFourthFunc(a,b) {
    return a*b
}
let z = myFourthFunc(2,5) // *5
document.getElementById("demo4").innerHTML = `The product is ${z}`

// Functions as objects

let body = "return Math.PI * rad * rad"
let circle = new Function("rad", body);
// let result = alert(circle(5));
let result2= circle(5)
document.getElementById("demo5").innerHTML = `The area of circ is ${result2}`


//Arrow Functions

const myFifthFunction = (a,b,c) => {
        return a*b*c
}

let product = myFifthFunction(2,3,4)
document.getElementById("demo6").innerHTML = `The result ${product}`

//Generator Functions

function* mySixthFunc(){
    yield 1;
    yield 2;
    return 3;
}
let generator = mySixthFunc();
let one = generator.next();
let two = generator.next();
let three = generator.next();
alert(JSON.stringify(one))
//{"value":1,"done":false}
alert(JSON.stringify(two))
// {"value":2,"done":false}
alert(JSON.stringify(three))
//{"value":3,"done":true}


let x1 = 5;
let y1 = 6;
let z1 = 7;
document.getElementById("demo8").innerHTML = "x + y = "+ eval("x1+y1");
document.getElementById("demo9").innerHTML = "z = "+ eval(z1);


console.log(eval(z1));
