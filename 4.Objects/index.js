let student = {
    name: "Chris",
    age: 21,
    studies: "Computer Science"
};
student.age = 25 // object properties
x = "studies" //object properties
document.getElementById("demo").innerHTML = student.name + " of the age  " + student.age + " studies " + student[x]


let nStudent = new Object();
student.name = "Satoshi",
student.age = 23,
student.studies = "Biology";

document.getElementById("demo2").innerHTML = student.name + " of the age  " + student.age + " studies " + student.studies



function stud(name, age, studies) {

    this.name = name;
    this.age = age;
    this.studies = studies;
 }
let studentConstruct = new stud("Matt", 24, "Law") 

 document.getElementById("demo3").innerHTML = studentConstruct.name + " of the age  " + studentConstruct.age + " studies " + studentConstruct.studies



 //OBJECT METHODS

 let user = {
    name: "Chris",
    age: 21
 }
function sayHi() {
    alert(`Hello ${user.name}`)
}

user.message = sayHi;

document.getElementById("demo4").innerHTML = `Hi ${user.name} `
document.getElementById("clickMe").onclick = user.message

//OBJECT ACCESSOR

let car = {
    model: "BMW 320d",
    color: "Navy Blue",
    fuel_type: "Diesel",
    
    get fuel(){
        return this.fuel_type
    }
};
document.getElementById("demo6").innerHTML = `Your car fuel is  ${car.fuel} `


//setter

let car2 = {
    model: "BMW 320d",
    color: "Navy Blue",
    fuel_type: "",
    
    set fuel(fuel){
        return this.fuel_type = fuel;
    }
};
car2.fuel = "Unleaded"
document.getElementById("demo7").innerHTML = `Your car fuel is  ${car2.fuel_type} `

