//Promise 3 States: Pending, Fullfilled,Rejected
//Once Promises is settled it becomes immutable and it cannot change it's state.
//Fullfilled : Asynchronous Action is PErformed, Rejected: Error Handling

let car = new Promise(function(resolve,reject){
    fuel_fullTank = false;
    if (fuel_fullTank) {
        resolve()
    } else {
        reject()
    }
})

car.then(function(){
   console.log("The fuel tank is full.");
}).catch(function(){
   console.log("The fuel tank is empty.")

})




let emptyTank = function(){
    return new Promise(function(resolve,reject){
        resolve(" The car doesn't have enough fuel ")
    })
}

let engine = function(msg){
    return new Promise(function(resolve,reject){
        resolve(msg +" The engine is overheating ")
    })
}

let travel = function(msg){
    return new Promise(function(resolve,reject){
        resolve(msg +" The car is not safe for travelling")
    })
}

emptyTank().then(function(result){
    return engine(result)
}).then(function(result){
    return travel(result)
}).then(function(result){
    console.log("Done!" + result);
})