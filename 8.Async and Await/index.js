// // W/o using async and await
// let result = function (score) {
//     return new Promise(function(resolve, reject){
//         console.log("Calculating results...");
//         if(score>50){
//             resolve("Congratulations! You have passed")
//         } else{
//             reject("You have failed")
//         }
//     })
// }
// let grade = function(response){
//     return new Promise(function (resolve, reject) {
//         console.log("Calculating your grade.. ");
//         resolve("Your grade is A. " + response) 
//     })
// }

// result(80).then(response =>{
//     console.log("Results recieved");
//     return grade(response)
// }).then(finalgrade =>{
//     console.log(finalgrade);
// }).catch(err =>{
//     console.log(err);
// })





console.log("");

// Using Async and Await

let result2 = function (score) {
    return new Promise(function(resolve, reject){
        console.log("Calculating results...");
        if(score>50){
            resolve("Congratulations! You have passed")
        } else{
            reject("You have failed")
        }
    })
}
let grade2 = function(response){
    return new Promise(function (resolve, reject) {
        console.log("Calculating your grade.. ");
        resolve("Your grade is A. " + response) 
    })
}

async function calculateResult(){
    try{
        const response = await result2(20)
        console.log("Results Received");
        const finalgrade = await grade2(response)
        console.log(finalgrade);
    }
    catch(err){
        console.log(err);
    }
}
calculateResult();
